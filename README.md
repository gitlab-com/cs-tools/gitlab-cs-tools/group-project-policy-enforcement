# Group project policy enforcement

Define a project in your group whose settings will be applied to all projects in the group.

## Limitations

Only settings accessible via the API can be applied. [The config.yml file](./config.yml) contains the list of attributes that will be queried in the policy project and applied to all other projects.

Will not apply changes to archived projects.

## Usage

`python3 apply_project_policy.py $GIT_TOKEN $GROUP_ID $POLICY_PROJECT_ID --dryrun $DRYRUN`

## Setup and execution

- Import as new project / fork repository.
- In Settings -> CI/CD -> Variables, add a variable "$GIT_TOKEN" with a GitLab API token as value. That token needs API read rights and permissions to read the group you want to report on.
- edit .gitlab-ci.yml
  - specify your group namespace or id in the $GROUP_ID variable
- Run the Pipeline

### Configuration

[The config.yml file](./config.yml) contains the set of attributes that can be queried in the policy project and applied to other projects. This set of attributes is limited by the API.

To be configurable, an attribute needs to be **both**:
* [Queryable in the policy project](https://docs.gitlab.com/ee/api/projects.html#get-single-project) as a project attribute
* [Editable in the dependent projects](https://docs.gitlab.com/ee/api/projects.html#edit-project)

Further, the attribute may **not** be an admin only functionality for use on GitLab.com (for example `repository_storage`).

Further, the attribute should make sense to apply to all projects, so `name`, `description`, `topics`, `tags` and all `mirror` settings are not included in the default [config.yml file](./config.yml).

**Adding new attributes**

If a new attribute is available in the API and has not yet been included in the [config.yml file](./config.yml) provided in this project, you can add it to your config file yourself.


## Disclaimer

This script is provided for educational purposes. It is **not supported by GitLab**. However, you can create an issue if you need help or propose a Merge Request. **Always run it in dryrun mode first, to understand the impact to your users.**

This script produces data file artifacts containing user names. Please use this script and its outputs with caution and in accordance to your local data privacy regulations.