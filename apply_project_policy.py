#!/usr/bin/env python3

import gitlab
import json
import csv
import os
import re
import sys
import argparse
import yaml
from junit_xml import TestSuite, TestCase
from distutils import util

def get_project_attributes(gl, projects):
    project_objects = []
    for project in projects:
        project_object = gl.projects.get(project)
        project_objects.append(project_object.attributes)
    return project_objects

def get_group_projects(gl, group):
    projects = []
    group_projects = group.projects.list(as_list=False, archived=False, include_subgroups=True)
    for group_project in group_projects:
        projects.append(gl.projects.get(group_project.id))
    return projects

def get_policy_project(gl, policy_project, group):
    project_path = policy_project
    #account for different ways to enter project ID
    if group.path not in policy_project:
        if not re.match("^[0-9]+$", policy_project):
            project_path = group.path + "/" + project_path
    try:
        return gl.projects.get(project_path)
    except Exception as e:
        print("[ERROR] Could not get policy project %s" % project_path)
        print(e)
        exit(1)

def align_protected_branches(gl, policy_protected_branches, project, config_attributes, dryrun):
    # TODO, this does not work yet because python gitlab does not support saving edited protected branches
    # need to manually assemble payload
    for policy_pb in policy_protected_branches:
        for project_pb in project.protectedbranches.list():
            if policy_pb.name == project_pb.name:
                policy_pb_data = {"name":policy_pb.name}
                mismatch = False
                for attribute in config_attributes["protected_branch_attributes"]:
                    policy_pb_data[attribute] = getattr(policy_pb, attribute)
                    if getattr(project_pb, attribute) != getattr(policy_pb, attribute):
                        mismatch = True
                        print("[WARN] Protected branch attribute mismatch: %s on %s in %s is %s, policy is %s" % (attribute, project_pb.name, project.path_with_namespace, getattr(project_pb, attribute), getattr(policy_pb, attribute)))
                if not dryrun and mismatch:
                    project.protectedbranches.create(policy_pb_data)

def apply_policy(gl, policy_project, projects, config_attributes, dryrun):
    policy_pr = policy_project.pushrules.get()
    policy_protected_branches = policy_project.protectedbranches.list()
    total_mismatch_count = 0
    misconfigured_projects_count = 0
    for project in projects:
        # policy project attributes
        is_misconfigured = False
        for attribute in config["project_attributes"]:
            if project.attributes[attribute] != policy_project.attributes[attribute]:
                print("[WARN] Project attribute mismatch: %s in %s is %s, policy is %s" % (attribute, project.path_with_namespace, project.attributes[attribute], policy_project.attributes[attribute]))
                total_mismatch_count += 1
                is_misconfigured = True
                if not dryrun:
                    print("Changing %s in %s from %s to %s" % (attribute, project.path_with_namespace, project.attributes[attribute], policy_project.attributes[attribute]))
                    setattr(project, attribute, policy_project.attributes[attribute])
        if is_misconfigured:
            project.save()
        # policy project push rules
        for attribute in config["push_rule_attributes"]:
            project_pr = project.pushrules.get()
            if getattr(project_pr, attribute) != getattr(policy_pr, attribute):
                total_mismatch_count += 1
                is_misconfigured = True
                print("[WARN] Push rule attribute mismatch: %s in %s is %s, policy is %s" % (attribute, project.path_with_namespace, getattr(project_pr, attribute), getattr(policy_pr, attribute)))
                if not dryrun:
                    print("Changing %s in %s from %s to %s" % (attribute, project.path_with_namespace, getattr(project_pr, attribute), getattr(policy_pr, attribute)))
                    setattr(project_pr, attribute, getattr(policy_pr, attribute))
                    project_pr.save()
        # no support for protected branches yet
        # align_protected_branches(gl, policy_protected_branches, project, config_attributes, dryrun)
        if is_misconfigured:
            misconfigured_projects_count += 1
    return misconfigured_projects_count, total_mismatch_count

parser = argparse.ArgumentParser(description='Apply one projects settings to all projects in a group')
parser.add_argument('token', help='API token able to read the requested group')
parser.add_argument('group', help='The group namespace / id to run this report on')
parser.add_argument('policy_project', help='The project namespace / id of the project whose settings will be applied to all')
parser.add_argument('--gitlab', help='GitLab instance ', default="https://gitlab.com")
parser.add_argument('--dryrun', help='Dryrun, only log mismatches', type=lambda x:bool(util.strtobool(x)))

args = parser.parse_args()
dryrun = args.dryrun
if not dryrun:
    print("This is NOT a dryrun, policy will be applied to all projects in group %s." % args.group)
else:
    print("This is a dryrun, only project settings diverging from policy will be logged.")

gl = gitlab.Gitlab(args.gitlab, private_token=args.token)

group = gl.groups.get(args.group)
try:
    group = gl.groups.get(args.group)
except error as e:
    print("[ERROR] can not get group %s" % args.group)
    print(e)
    exit(1)

config = {}
with open("config.yml", "r") as configfile:
    config = yaml.safe_load(configfile)

projects = []

# writing a manual counter here because status is not easy to get from test cases
misconfigured_projects_count = 0

policy_project = get_policy_project(gl, args.policy_project, group)
projects = get_group_projects(gl, group)

stats = apply_policy(gl, policy_project, projects, config, dryrun)

print("Found %s misconfigured projects" % stats[0])
print("Found %s misconfigured attributes" % stats[1])
